import mongoose from 'mongoose';

const playersSchema = mongoose.Schema({
    name: String,
    position: String,
    thumbnail: String,
    signin: {
      amount: Number,
      currency: String,
    },
    born: Date,
})
const Players = mongoose.model('Players', playersSchema);

export default Players;