import express from 'express';
let router = express.Router();

import Leagues from '../models/leagues.js'
import Teams from '../models/teams.js'
import Players from '../models/players.js'
import ApiController from '../controlleurs/api-controller.js';

// Endpoint to get teams by league name 
/**
 * @swagger
 * tags:
 *   name: Leagues
 *   description: API endpoints for managing leagues
 */

/**
 * @swagger
 * /teams?search=:leagueName:
 *   get:
 *     summary: Get list of teams by league name
 *     tags: [teams]
 *     parameters:
 *       - in: query
 *         name: search
 *         required: true
 *         description: league search query
 *         schema:
 *           type: string
 *       - in: query
 *         name: page
 *         required: false
 *         description: current page searched 
 *         schema:
 *           type: number
 *     responses:
 *       200:
 *         description: Successful response
 *       400:
 *         description: bad request
 *       204:
 *         description: No Content 
 *       500:
 *         description: internal server
 */

router.get('/teams', ApiController.getTeamsByLeague ) ;



export default  router;
