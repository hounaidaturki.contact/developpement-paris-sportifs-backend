import mongoose from "mongoose";
const leaguesSchema = mongoose.Schema({
  name: String,
  sport: String,
  teams: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Teams",
    },
  ],
  // Add pagination fields
  totalPages: {
    type: Number,
    default: 1,
  },
  currentPage: {
    type: Number,
    default: 1,
  },
});
const Leagues = mongoose.model("Leagues", leaguesSchema);

export default Leagues;
