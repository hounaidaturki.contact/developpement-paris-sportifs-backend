import status from "http-status";
import Constants from "../utils/constants.js";
import ApiService from ".././services/api-service.js";

// Define the controller class
export default class ApiController {
  // Method to handle a getTeamsByLeague request
  static async getTeamsByLeague(req, res) {
    const { search ,page} = req.query;
    try {
      if (!search) {
        res.status(status.BAD_REQUEST).json({
          status: Constants.FAIL,
          message: Constants.REQUIRED_ATTRIBUTES,
        });
        return;
      }

      const data = await ApiService.getTeamsByLeague(search,page);

      if (!data) {
        res.status(status.NO_CONTENT).send(); // Send 204 No Content
      } else {
        res.status(status.OK).json({
          status: Constants.SUCCESS,
          data: data,
        });
      }

      
       
    } catch (error) {
      res.status(status.INTERNAL_SERVER_ERROR).json({
        status: Constants.ERROR,
        message: error.message,
      });
    }
  }
}
