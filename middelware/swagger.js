import swaggerJSDoc from'swagger-jsdoc';

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Paris Sport REST API'  ,
      version: '1.0.0',
      description: 'API documentation for test',
    },
    servers: [
      {
        url: 'http://localhost:3000', // Update with your server URL
      },
    ],
  },
  apis: ["./routes/*.js"], // Update with the path to your main Express app file
};

const swaggerConfig = swaggerJSDoc(options);



export default swaggerConfig;
