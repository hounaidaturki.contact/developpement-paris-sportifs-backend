import status from 'http-status'  
import express from 'express';
import apiRoute from './routes/api.route.js' ; 
import environment from './environment.js';
import swaggerUi from 'swagger-ui-express';
import swaggerConfig from'./middelware/swagger.js';
import connectToDatabase from'./middelware/db.config.js';

import cors from 'cors';
const app = express();

connectToDatabase();

app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerConfig));
app.use('/api', apiRoute);
app.use('*', (req, res) => {
  res.status(status.NOT_FOUND).json({
      error: "Not found"
  })
})

app.listen(environment.PORT, () => {
  console.log(`Server is running on http://localhost:${environment.PORT}`);
});

