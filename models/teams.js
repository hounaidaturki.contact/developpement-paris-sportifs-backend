import mongoose from 'mongoose';

const teamsSchema = mongoose.Schema({

    name: String,
    thumbnail: String,
    players: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Players',
      },
    ],
  
})
const Teams = mongoose.model('Teams', teamsSchema);

export default Teams;