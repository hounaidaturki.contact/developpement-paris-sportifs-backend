import Leagues from "../models/leagues.js";
const ITEMS_PER_PAGE = 2;

export default class ApiService {
  static async getTeamsByLeague(search, page = 1) {
    return new Promise(async (resolve, reject) => {
      try {
        const league = await this.findLeagueWithTeamsAndPlayers(search, page);
        // Check if the league was found
        if (league) {
          resolve(league);
        } else {
          resolve();
        }
      } catch (error) {
        reject(new Error(`Error getting teams by league: ${error.message}`));
      }
    });
  }

  //  function to find league by name with teams and players
  static async findLeagueWithTeamsAndPlayers(leagueName, page = 1) {
    const skipItems = (page - 1) * ITEMS_PER_PAGE;

    try {
      const league = await Leagues.findOne({
        name: { $regex: new RegExp(leagueName, "i") },
      })
        .populate({
          path: "teams",
          options: {
            skip: skipItems,
            limit: ITEMS_PER_PAGE,
          },
          populate: {
            path: "players",
            model: "Players",
          },
        })
        .exec();

      // Calculate pagination details
      if (league && league.teams) {
        const totalTeams = league.teams.length;
        league.totalPages = Math.ceil(totalTeams / ITEMS_PER_PAGE);
        league.currentPage = page;
        
       // Adjust total pages if there is a remainder
  if (totalTeams % ITEMS_PER_PAGE === 0) {
    league.totalPages++;
  }
      }


      return league;
    } catch (error) {
      console.error(error);
      throw new Error("An error occurred while fetching league data.");
    }
  }
}
